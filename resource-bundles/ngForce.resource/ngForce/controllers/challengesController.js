app.controller('challengesController', function($scope, $rootScope, $dialog, vfr) {

	var pChallengeQuery = vfr.query("SELECT id, Name, City__c, State__c, Description__c, Photo__c FROM Challenge__c");
	pChallengeQuery.then(function(d) {
		$scope.challenges = d.records;
		if(!$scope.$$phase) {
			$scope.$digest();
		}
	});

	// If our promise fails to resolve do this.
	pChallengeQuery.fail(function(e){log(e);});

	$scope.$on('UpdateChallenges', function(newValue, parameters) {
		var pChallengeQuery;

		if(parameters['deleted']) {
			$scope.challenges = _.filter($scope.challenges, function(obj){
				return (obj.Id != parameters.deleted.Id);
			});
			if(!$scope.$$phase) {
				$scope.$digest();
			}
		} else if (parameters['Update']) {
			pChallengeQuery = vfr.query("SELECT id, Name, City__c, State__c, Description__c, Photo__c"+
																		" FROM Challenge__c WHERE ID = '" + parameters.Update + "'");
			$scope.challenges = _.filter($scope.challenges, function(obj){
				return (obj.Id != parameters.Update);
			});
		} else {
			pChallengeQuery = vfr.query("SELECT id, Name, City__c, State__c, Description__c, Photo__c"+
																		" FROM Challenge__c WHERE ID = '" + parameters.Id + "'");
		}

		pChallengeQuery.then(function(d) {
			$scope.challenges = _.union($scope.challenges, d.records);
			if(!$scope.$$phase) {
				$scope.$digest();
			}
		});

	});

	$scope.openDetailsDialog = function(challenge){
		var dialogOpts = {
			backdrop: true,
			keyboard: true,
			backdropClick: true,
			templateUrl: '/apex/ChallengeDetailsTemplate',
			controller: 'ChallengeDetailsController',
			resolve: {
				challenge: function() {
					return angular.copy(challenge);
				}
			}
		};

		var d = $dialog.dialog(dialogOpts);

		d.open().then(function(result){
			if(result){}
		});
	};


});