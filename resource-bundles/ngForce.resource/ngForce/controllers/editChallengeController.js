app.controller('editChallengeController', function($scope, $rootScope, $dialog, dialog, challenge, vfr) {
	
	if(challenge) {
		$scope.title = "Edit Challenge";
		$scope.City__c = challenge.City__c;
		$scope.State__c = challenge.State__c;
		$scope.Description__c = challenge.Description__c;
		$scope.Name = challenge.Name;
		if(!$scope.$$phase) {
			$scope.$digest();
		}
	}

	$scope.close = function(result){

		var fields = {'Name' : $scope.Name,
									'City__c' : $scope.City__c,
									'State__c' : $scope.State__c,
									'Description__c' : $scope.Description__c
								};

		var pUpdateChallenge = vfr.update('Challenge__c', challenge.Id, fields );
		pUpdateChallenge.then(function(d) {
			$rootScope.$broadcast('UpdateChallenges', {'Update' : challenge.Id});
		});
		pUpdateChallenge.fail(function(f){log(f);});
		dialog.close(result);
	};
});