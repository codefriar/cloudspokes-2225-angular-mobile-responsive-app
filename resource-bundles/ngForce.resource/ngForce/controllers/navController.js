app.controller('navController', function($scope, $dialog, vfr) {

  $scope.Description__c = "Describe your Awesome Challenge";

	$scope.openNewDialog = function(){
		var dialogOpts = {
			backdrop: true,
			keyboard: true,
			backdropClick: true,
			templateUrl: '/apex/NewChallengeTemplate',
			controller: 'newChallengeController'
		};

		var n = $dialog.dialog(dialogOpts);

		n.open().then(function(result){
			if(result){}
		});
	};
});