app.controller('ChallengeDetailsController', function($scope, $rootScope, $dialog, dialog, challenge, vfr) {
	$scope.challenge = challenge;

	$scope.close = function(result){
		dialog.close(result);
	};

	$scope.editChallenge = function(challenge) {

		dialog.close(null);
		var dialogOpts = {
			backdrop: true,
			keyboard: true,
			backdropClick: true,
			templateUrl: '/apex/NewChallengeTemplate',
			controller: 'editChallengeController',
			resolve: {
				challenge: function() {
					return angular.copy(challenge);
				}
			}
		};

		var n = $dialog.dialog(dialogOpts);

		n.open().then(function(result){
			if(result){}
		});
	};

	$scope.deleteChallenge = function(id) {
		dialog.close(null);
		var title = 'Deletion Warning!';
		var msg = 'Are you sure you want to delete ' + challenge.Name;
		var btns = [{result:'cancel', label: 'Cancel'}, {result:'delete', label: 'Yes, Delete!', cssClass: 'btn-danger'}];

		$dialog.messageBox(title, msg, btns)
			.open()
			.then(function(result){
				if(result == 'delete') {
					var pDelete = vfr.del('Challenge__c', challenge.Id);
					pDelete.then(function(r){
						$rootScope.$broadcast('UpdateChallenges', {'deleted' : challenge});
					});
				}
		});
	};

	$scope.openMessageBox = function(){

	};

});