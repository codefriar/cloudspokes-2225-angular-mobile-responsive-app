app.controller('newChallengeController', function($scope, $rootScope, $dialog, dialog, vfr) {
	$scope.title = "New Challenge";
	$scope.close = function(result){

		var fields = {'Name' : $scope.Name,
									'City__c' : $scope.City__c,
									'State__c' : $scope.State__c,
									'Description__c' : $scope.Description__c
								};

		var pSaveNewChallenge = vfr.create('Challenge__c', fields );
		pSaveNewChallenge.then(function(d) {
			$rootScope.$broadcast('UpdateChallenges', {'Id' : d.id});
		});

		dialog.close(result);
	};
});